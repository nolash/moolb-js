let moolb = require('../moolb/moolb');

module.exports = {
	Bloom: moolb.Bloom,
	fromBytes: moolb.fromBytes,
};
