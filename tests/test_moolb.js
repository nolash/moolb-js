let moolb = require('../moolb/moolb');
let assert = require('assert');

describe('check', () => {
	it('builtin', async () => {
		let ref = b = new moolb.Bloom(8192 * 8, 3);
		let enc = new TextEncoder();

		let v = enc.encode('1024');
		await b.add(v);
		let r = await b.check(v);
		assert(r);

		v = enc.encode('1023');
		r = await b.check(v);
		assert(!r)
	});
});
