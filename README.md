# moolb.js

Bloom filter for python with pluggable hasher backend

## USAGE

```
let assert = require('assert');

let ref = b = new moolb.Bloom(8192 * 8, 3);
let enc = new TextEncoder();

let v = enc.encode('1024');
b.add(v);
let r = b.check(v);
assert(r);

v = enc.encode('1023');
r = b.check(v);
assert(!r)
```

## LICENSE

GPL3
